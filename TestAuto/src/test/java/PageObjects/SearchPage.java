package PageObjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import Utility.Base;
import Utility.ExtentTestManager;
import Utility.util;

public class SearchPage extends Base{
	
	public static String productNamesXpath = "//figcaption/a[contains(@class,'product-name')]/span";
	
	
	public static boolean matchSearchWithProductName(String searchName,String resultsXpath){
		boolean match = false;
		String actualProdName = null;
		
		List<WebElement> prodNames = new ArrayList<WebElement>();
		
		prodNames = util.getElements(resultsXpath);
		 
		  for(WebElement prodName: prodNames){
			  
			  actualProdName =   prodName.getText();
			  			  
		    if(actualProdName.contains(searchName)){
		    	match = true;
		      }else{
		        match = false; 
		        ExtentTestManager.getTest().log(LogStatus.ERROR, "Following product is found not matching with search and there can be more."+actualProdName);
		    	break;
		      }
		   }

		
		return match;
	}

}
