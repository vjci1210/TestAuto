package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import Utility.Base;
import Utility.ExtentTestManager;

public class HomePage extends Base {
	
	private static String searchInputXpath = "//input[@id = 'search_query_top']";
	private static String searchButtonXpath = "//button[@class='button btn btn-default button-search']";
	public static String autoCompleteResultsNameXpath = "//div[contains(@class,'jolisearch-description')]/span[@class='jolisearch-name']/strong";
	
	
	public static void enterSearch(String input){
		
		getDriver().findElement(By.xpath(searchInputXpath)).click();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getDriver().findElement(By.xpath(searchInputXpath)).sendKeys(input);
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Entered text in search input field.");
		
	}
	
	public static void clickSearchButton(){
		getDriver().findElement(By.xpath(searchButtonXpath)).click();
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Clicked on search button");
	}
	
	public static void searchByEnterKey(){
		getDriver().findElement(By.xpath(searchInputXpath)).sendKeys(Keys.ENTER);
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Enter key pressed.");
	}

}
