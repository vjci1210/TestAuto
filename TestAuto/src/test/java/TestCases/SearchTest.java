package TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import PageObjects.HomePage;
import PageObjects.SearchPage;
import Utility.Base;
import Utility.ExtentTestManager;
import Utility.util;


public class SearchTest extends Base{
	
	
  @Test (groups = {"Sanity","Regression"})
  public void verifySearchWithSearchButton() {
   
	 boolean status= false;
	 String searchProductName = util.getTestData("searchText");
	 
	 HomePage.enterSearch(searchProductName);
	 
	 HomePage.clickSearchButton();
	 
	
	 
	 status = SearchPage.matchSearchWithProductName(searchProductName,SearchPage.productNamesXpath);
	 
	 if(status){
		  ExtentTestManager.getTest().log(LogStatus.PASS, "Search results are matching with search keyword.");
	  }else{
		  ExtentTestManager.getTest().log(LogStatus.FAIL, "Search results doesn't match with search keyword.");
		  Assert.fail();
	  }
	
	 
  }
  
  @Test (groups = {"Regression"})
  public void verifySearchWithEnterKey() {
	  
	 boolean status= false;
	 String searchProductName = util.getTestData("searchText");
	 
	 HomePage.enterSearch(searchProductName);
	 
	 HomePage.searchByEnterKey();
	 
	 status = SearchPage.matchSearchWithProductName(searchProductName,SearchPage.productNamesXpath);
	 
	 if(status){
		  ExtentTestManager.getTest().log(LogStatus.PASS, "Search results are matching with search keyword.");
	  }else{
		  ExtentTestManager.getTest().log(LogStatus.FAIL, "Search results doesn't match with search keyword.");
		  Assert.fail();
	  }
	  
	 
  }
  
  
  @Test (groups = {"Sanity","Regression"})
  public void verifyAutoCompleteSearch() {
   
  boolean status= false;
  String searchProductName = util.getTestData("searchText");
  
  HomePage.enterSearch(searchProductName);
  
  util.explicitWait(HomePage.autoCompleteResultsNameXpath, 3);
  
  status = SearchPage.matchSearchWithProductName(searchProductName,HomePage.autoCompleteResultsNameXpath);
  
  if(status){
    ExtentTestManager.getTest().log(LogStatus.PASS, "AutoComplete search is working fine.");
   }else{
    ExtentTestManager.getTest().log(LogStatus.FAIL, "AutoComplete results list is different from search keyword.");
    Assert.fail();
   }
 
  
  }

    
}
