package Utility;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Base {
    private static ExtentReports extent;
    private static ExtentTest test;
    //public static WebDriver driver;
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
    
   
   
    private void setDriver(String browserType, String appURL) {
      WebDriver driver = null;	
      
      
      switch(browserType){
   	  case "chrome":
   		 ExtentTestManager.getTest().log(LogStatus.INFO, "Launching Chrome browser"); 
   	     System.setProperty("webdriver.chrome.driver","chromedriver_New.exe");
   	     driver = new ChromeDriver();
   	     break;
   	  
   	  case "firefox":
   		ExtentTestManager.getTest().log(LogStatus.INFO, "Launching Firefox browser"); 
    	System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		driver = new FirefoxDriver();
		
       	  
        }
		setWebDriver(driver);
		
		getDriver().manage().window().maximize();		   
	    getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	    getDriver().get(appURL);
		
		
	}
    
    
    protected static WebDriver getDriver() {
        return webDriver.get();
    }
 
    protected static void setWebDriver(WebDriver driver) {
    	webDriver.set(driver);
    }
   

    @BeforeSuite (groups = {"Sanity","Regression"})
    public void extentSetup(ITestContext context) {
        ExtentManager.setOutputDirectory(context);
        extent = ExtentManager.getInstance();
    }
    
    @BeforeMethod (groups = {"Sanity","Regression"})
    @Parameters({"browser"})
	public void initializeTestBaseSetup(String browser,Method method) {
		try {
			 ExtentTestManager.startTest(method.getName());
			setDriver(browser, util.getConfigValue("appUrl"));

		} catch (Exception e) {
			System.out.println("Error....." + e.getStackTrace());
		}
	}
    
   
   

    protected String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }

    @AfterMethod (groups = {"Sanity","Regression"})
    @Parameters({"browser"})
    public void afterEachTestMethod(ITestResult result,String browser) {
    	  ExtentTestManager.getTest().getTest().setStartedTime(getTime(result.getStartMillis()));  // new
          ExtentTestManager.getTest().getTest().setEndedTime(getTime(result.getEndMillis()));  // new

          for (String group : result.getMethod().getGroups()) {
              ExtentTestManager.getTest().assignCategory(group);  // new
          }

          if (result.getStatus() == 1) {
              ExtentTestManager.getTest().log(LogStatus.PASS, "Test Passed");  // new
          } else if (result.getStatus() == 2) {
              String path = util.getscreenshot(getDriver(), result.getName());
             
              if(browser.equalsIgnoreCase("firefox")){
              String editPath = "file://";
              String FinalPath = editPath.concat(path);
              
              System.out.println("Path in gecko refreshed");
             
              String image = ExtentTestManager.getTest().addScreenCapture(FinalPath);
          	ExtentTestManager.getTest().log(LogStatus.FAIL,getStackTrace(result.getThrowable()));
          	try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
          	ExtentTestManager.getTest().log(LogStatus.FAIL,image);
          	getDriver().get(FinalPath);
          	
              }else{
              	
                  String image = ExtentTestManager.getTest().addScreenCapture(path);
               	ExtentTestManager.getTest().log(LogStatus.FAIL,getStackTrace(result.getThrowable()));
               	ExtentTestManager.getTest().log(LogStatus.FAIL,image);
               	getDriver().get(path);
              }
          	
          } else if (result.getStatus() == 3) {
              ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");  // new
          }

          ExtentTestManager.endTest();  // new
     
          extent.flush();
          if(getDriver()!=null){
            	 
          	getDriver().close();
          	 
           }
          
          webDriver.set(null);  
    }

    @AfterSuite (groups = {"Sanity","Regression"})
    public void generateReport() {
        extent.close();
    }

    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }
}
