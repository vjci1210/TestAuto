package Utility;


import java.awt.Robot;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;
import com.relevantcodes.extentreports.LogStatus;

import PageObjects.HomePage;

import org.apache.poi.xssf.usermodel.XSSFCell;

import org.apache.poi.xssf.usermodel.XSSFRow;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class util extends Base {
	
	protected static FileReader reader;
	protected static Properties properties;
	
	protected static XSSFWorkbook ExcelWBook;
	protected static XSSFSheet ExcelWSheet;
	protected static XSSFRow Row;
	protected static XSSFCell Cell;
	private static String [][] arrayExcelData;
	
	
	
	

	
	public static String getConfigValue(String key){
		String val=null;
		try {
			reader = new FileReader("config.properties");
			properties = new Properties();
			properties.load(reader);
			val = properties.getProperty(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return val;
	}
	
	
	public static String getTestData(String key){
		String val=null;
		try {
			reader = new FileReader(util.getConfigValue("testData"));
			properties = new Properties();
			properties.load(reader);
			val = properties.getProperty(key);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FATAL, "Testdata file is missing");
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		return val;
	}
	
	//This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method
	 
		public static void setExcelFile(String Path,String SheetName) throws Exception {

				try {

	   			// Open the Excel file

				FileInputStream ExcelFile = new FileInputStream(Path);

				// Access the required test data sheet

				ExcelWBook = new XSSFWorkbook(ExcelFile);

				ExcelWSheet = ExcelWBook.getSheet(SheetName);

				} catch (Exception e){

					throw (e);

				}

		}
		
		
		public static String[][]  get2DExcelData(String path, String sheetName){
			arrayExcelData = null;
			try{
				
			setExcelFile(path,sheetName);
			XSSFRow row = ExcelWSheet.getRow(0);
			int totalRows = ExcelWSheet.getLastRowNum();
			int totalColumns = row.getLastCellNum();
			System.out.println("last column no. is: "+totalColumns);
			
			arrayExcelData = new String[totalRows-1][totalColumns];
			
			for(int i=1;i<=totalRows;i++){
				
				for(int j=0;j<totalColumns;j++){
					Cell = ExcelWSheet.getRow(i).getCell(j);
					arrayExcelData[i-1][j] = Cell.getStringCellValue();
					
				}
			}
			}catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return arrayExcelData;
		} 

		//This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num

	    public static String getCellData(int RowNum, int ColNum) throws Exception{

				try{
					
	  			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
	  			
	  			int length = Cell.getStringCellValue().length();
	  			
	  			String CellData = Cell.getStringCellValue();
	  			return CellData; 			

	  			}catch (Exception e){

					return"";

	  			}

	    }
	
	public static String getscreenshot(WebDriver driver,String screenshotName)
    {    
		String filePath=null;
		try{
            File scrnFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            filePath  = System.getProperty("user.dir")+"\\Screenshots\\"+screenshotName+".png";
            
            FileUtils.copyFile(scrnFile, new File(filePath));
            
    }catch(Exception e){
    	   e.printStackTrace();
    }
    
	return filePath;
    }
	
	
	public static void explicitWait(String locator,int timeInSeconds){
		
		WebDriverWait wait=new WebDriverWait(getDriver(), timeInSeconds);
		  
	    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
	}
	
	public static void clickByXpath(String xpath){
		getDriver().findElement(By.xpath(xpath)).click();
	}
    
	public static String  replaceChar(String testChar,String initial,String replacedChar){
		String newstr = testChar.replace(initial, replacedChar);
		
		return newstr;
	}
	
	public static void mousehover(String st1){
	    Actions a = new Actions(getDriver());
	    WebElement we =   getDriver().findElement(By.xpath(st1));
	    a.moveToElement(we).build().perform();
	}

	public static String getText(String xpath){
		String text=null;
		text = getDriver().findElement(By.xpath(xpath)).getText();
		return text;
	}
	

	public static String getAttribute(String attribute,String xpath){
		String attributeVal = null;
		attributeVal = getDriver().findElement(By.xpath(xpath)).getAttribute(attribute);
		return attributeVal;
	}
	
	public static void enterText(String data, String xpath){
		getDriver().findElement(By.xpath(xpath)).sendKeys(data);
	}
	
	public static void clearText(String xpath){
		getDriver().findElement(By.xpath(xpath)).clear();
	}

	public static void selectDropDownValue(String text,String xpath){
		WebElement we = getDriver().findElement(By.xpath(xpath));
		Select selectDrop = new Select(we);
		selectDrop.selectByVisibleText(text);
	}
	
	public static List<WebElement> getElements(String xpath){
		List<WebElement> ls = new ArrayList<WebElement>();
		 ls = getDriver().findElements(By.xpath(xpath));
		
		return ls;
	}
	
	public static void scrollTo() {
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("window.scrollBy(0,550)", "");
    }
	
	public static void switchToIframe(String frameid){
		
		getDriver().switchTo().frame(frameid);
	}
	public static boolean elementPresentOrNot(String xpath){
 	   boolean status=false;
 	   status = getDriver().findElement(By.xpath(xpath)).isDisplayed();
 	   return status;
    }

	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}


    
    public static String lower_casestring(String st1) {
    	st1= st1.toLowerCase();
    	return st1;
    }
    public static String Replace_space_string(String st1) {
    	st1= st1.replaceAll("\\s+$", "");
    	return st1;
    }


    public static String replacecharachter(String s){
    	String numberOnly= s.replaceAll("[^0-9]", "");
    	double i = (Double.parseDouble(numberOnly))/100;
    	int k = (int) i;
    	String numbero = Integer.toString(k);
    	return numbero;
    }
    
    public static void selectdropdownvalue(String str1, String str2){
    	
    	 Select daydropdown = new Select(getDriver().findElement(By.xpath(str1)));
    	 int i= Integer.parseInt(str2);
    	 daydropdown.selectByIndex(i); 
    }

   

    public static void clickevent(String str){
    
    	getDriver().findElement(By.xpath(str)).click();
    }
    
    public static void clickIDevent(String str){
        
    	getDriver().findElement(By.id(str)).click();
    }
    
    public static void pagescroll(int i){
    	JavascriptExecutor jse = (JavascriptExecutor)getDriver();
    	jse.executeScript("window.scrollBy(0,"+i+")","");
    	    	
    }
   

   
    public static void enterdata(String str, String str1){
    	
    	getDriver().findElement(By.xpath(str)).sendKeys(str1);
    	
    }
    
    
    
   public static void enterdataID(String str, String str1){
    	
	   getDriver().findElement(By.id(str)).sendKeys(str1);
    	
    }


  public static boolean elementdisplayed(String str){
	  getDriver().findElement(By.xpath(str)).isDisplayed();
	  return true;
  }
  
  public static void getelement(String stname, String stelement){
	  String str;
	  
	  List<WebElement> drop = getDriver().findElements(By.xpath(stelement));
		
		 java.util.Iterator<WebElement> i = drop.iterator();
		
		 while(i.hasNext()) {
			    WebElement row = i.next();
			     str = row.getText();
			
			    if(str.equalsIgnoreCase(stname)){
				   ExtentTestManager.getTest().log(LogStatus.INFO, "clicking on product " + str);
				   row.click();
			    		break;
			    		}
			}
	  
  }
  
 
  

  
  public static String getattribute(String xpath, String att){
	 
	 String st = getDriver().findElement(By.xpath(xpath)).getAttribute(att);
	return st;
  }
  
  public static boolean guiMatchFind(String imagePath){
	    boolean match = false;
	    System.out.println("Image pathh is: "+imagePath);
		Screen screen = new Screen();
		
		try {
		    Pattern image = new Pattern(imagePath);
		    screen.wait(image,3);
			
			screen.find(image);
			
			System.out.println("Image found");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Image match found.");
			match = true;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ExtentTestManager.getTest().log(LogStatus.INFO, "Image match not found.");
			e.printStackTrace();
		}
	 
	  return match;
  }
  
  
  public static boolean guiMatchClick(String imagePath){
	    boolean match = false;
	    System.out.println("Image pathh is: "+imagePath);
		Screen screen = new Screen();
		
		try {
		    Pattern image = new Pattern(imagePath);
		    screen.wait(image,3);
			
		    screen.click(image);
			System.out.println("Image found and clicked");
			
			match = true;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ExtentTestManager.getTest().log(LogStatus.INFO, "Image match not found.");
			e.printStackTrace();
		}
	 
	  return match;
}
  
  
 
  
}
